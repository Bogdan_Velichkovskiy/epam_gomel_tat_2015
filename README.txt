Home tasks repository of Epam Gomel TAT 2015 training.
Workflow for submitting home task :
fork ksimanenka/EPAM_GOMEL_TAT_2015 (https://bitbucket.org/ksimanenka/epam_gomel_tat_2015) > 
	pull repo > 
		do home task > 
			commit to local repo > 
				push to forked repo in bitbucket > 
					create pull request to ksimanenka/EPAM_GOMEL_TAT_2015 (https://bitbucket.org/ksimanenka/epam_gomel_tat_2015) 
