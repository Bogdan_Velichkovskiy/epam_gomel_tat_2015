package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by user on 20.03.15.
 */
public class LoginToMailboxTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @Test(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @AfterClass(description = "Logout...")
    public void logout() {
        loginGuiService.logout(defaultAccount);
    }
}
