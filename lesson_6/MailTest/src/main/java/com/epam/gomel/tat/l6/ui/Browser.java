package com.epam.gomel.tat.l6.ui;

import com.epam.gomel.tat.l6.reporting.Logger;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.sun.istack.internal.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.gomel.tat.l6.reporting.Constants.BrowserOperationsMessagePatterns.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 20.03.15.
 */
public class Browser {

    static final String DOWNLOAD_DIR = "d:\\_TEMP\\";
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, "
            + "application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, "
            + "application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 30;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int AJAX_TIMEOUT = 20;
    private static final int WAIT_ELEMENT_TIMEOUT = 30;

    private WebDriver driver;

    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        synchronized (Browser.class) {
            if (instance != null) {
                return instance;
            }
            return instance = init();
        }
    }

    private static Browser init() {
        Logger.info(INITIALIZING_BROWSER);
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        Logger.info(CONFIGURING_PROFILE);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String url) {
        Logger.info(String.format(OPENING_URL_PATTERN, url));
        driver.get(url);
    }

    public void kill() {
        Logger.info(CLOSING_BROWSER);
        driver.quit();
    }

    public void click(By locator) {
        Logger.info(String.format(CLICK_PATTERN, locator));
        driver.findElement(locator).click();
    }

    public void goTo(String href) {
        Logger.info(String.format(NAVIGATE_PATTERN, href));
        driver.navigate().to(href);
    }

    public void type(By locator, String text) {
        Logger.info(String.format(TYPE_TEXT_PATTERN, text, locator));
        driver.findElement(locator).sendKeys(text);
    }

    public void clearElementText(By locator) {
        driver.findElement(locator).clear();
    }

    public String getElementText(By locator) {
        Logger.info(String.format(GETTING_TEXT_PATTERN, locator));
        return isPresent(locator) ? driver.findElement(locator).getText() : null;
    }

    public String getElementAttribute(By locator, String attribute) {
        Logger.info(String.format(GETTING_ATTRIBUTE_PATTERN, attribute, locator));
        return driver.findElement(locator).getAttribute(attribute);
    }

    public boolean isPresent(By locator) {
        Logger.info(String.format(LOCATING_FOR_ELEMENTS_PATTERN, locator));
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info(String.format(WAITING_FOR_PRESENT_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info(String.format(WAITING_FOR_VISIBLE_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public boolean isElementVisible(By locator) {
        Logger.info(String.format(CHECKING_FOR_VISIBLE_PATTERN, locator));
        if (isPresent(locator)) {
            WebElement element = driver.findElement(locator);
            return element.isDisplayed();
        }
        return false;
    }

    public void waitForInvisible(By locator) {
        Logger.info(String.format(WAITING_FOR_INVISIBLE_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info(String.format(PROCESSING_AJAX_WITH_TIMEOUT_PATTERN, AJAX_TIMEOUT));
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void waitForFileDownload(String filePath) {
        Logger.info(String.format(WAITING_FOR_DOWNLOADING_PATTERN, filePath));
        File downloadedFile = new File(filePath);
        FluentWait wait = new FluentWait<File>(downloadedFile).withTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Nullable
            @Override
            public Boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public void submit(By locator) {
        Logger.info(String.format(SUBMIT_FORM_PATTERN, locator));
        driver.findElement(locator).submit();
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getDownloadDir() {
        return DOWNLOAD_DIR;
    }
}