package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import com.epam.gomel.tat.l6.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.epam.gomel.tat.l6.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by user on 22.03.15.
 */
public class MarkAsNotSpamMailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = null;
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() throws FileNotFoundException {
        letter = generateAndGetLetter(defaultAccount.getEmail());
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Mark mail as spam", dependsOnMethods = "sendMail")
    public void markMailAsSpam() {
        mailGuiService.markMailAsSpam(letter);
    }

    @Test(description = "Mark mail as not spam", dependsOnMethods = "markMailAsSpam")
    public void markMailAsNotSpam() {
        mailGuiService.markMailAsNotSpam(letter);
        Assert.assertFalse(mailGuiService.ifMailInSpamList(letter),"The message remained in spam");
    }

    @Test(description = "Check mail in spam list", dependsOnMethods = "markMailAsNotSpam")
    public void checkMailInInboxList() {
        mailGuiService.checkMailInInboxList(letter);
    }

    @AfterClass(description = "Logout...")
    public void logout() {
        loginGuiService.logout(defaultAccount);
    }
}
