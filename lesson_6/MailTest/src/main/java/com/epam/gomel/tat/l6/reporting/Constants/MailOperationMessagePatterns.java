package com.epam.gomel.tat.l6.reporting.Constants;

/**
 * Created by user on 24.03.15.
 */
public class MailOperationMessagePatterns {
    public static final String ERROR_RECIEVERS_NOT_IDENTICAL =
            "Initial reciever and reciever in letter are not identical: '%s' and '%s'!";
    public static final String ERROR_SUBJECTS_NOT_IDENTICAL =
            "Initial subject and subject in letter are not identical: '%s' and '%s'!";
    public static final String ERROR_CONTENTS_NOT_IDENTICAL =
            "Initial content and content in letter are not identical!: '%s' and '%s'";
    public static final String ERROR_FILE_CONTENTS_NOT_IDENTICAL =
            "Initial file content and file content in letter are not identical: '%s' and '%s'!";
    public static final String OPENING_LETTER_CONTENT_PAGE = "Opening letter content page.";
    public static final String COMPARISON_OF_LETTERS_SUCCESSFULLY_FINISHED = "Comparison of letters content successfully finished.";
    public static final String COMPARE_LETTER_CONTENT_PATTERN = "Compare letter content '%s' with '%s'.";
    public static final String CHECKING_FOR_LETTER_REMAINED_IN_SPAM_PATTERN = "Checking for letter '%s' remained in spam list.";
    public static final String MARK_LETTER_AS_NOT_SPAM_PATTERN = "Mark letter '%s' as not spam.";
    public static final String MARK_LETTER_AS_SPAM_PATTERN = "Mark letter '%s' as spam.";
    public static final String DELETING_LETTER_FROM_INBOX_PATTERN = "Deleting letter '%s' from inbox.";
    public static final String RETRIEVING_LETTER_FROM_LETTER_CONTENT_PAGE = "Retrieving letter from letter content page.";
    public static final String CHECKING_LETTER_IN_SPAM_LIST_PATTERN = "Checking letter '%s' in spam list.";
    public static final String CHECKING_LETTER_IN_TRASH_LIST_PATTERN = "Checking letter '%s' in trash list.";
    public static final String CHECKING_LETTER_IN_SENT_LIST_PATTERN = "Checking letter '%s' in sent list.";
    public static final String CHECKING_LETTER_IN_INBOX_LIST_PATTERN = "Checking letter '%s' in inbox list.";
    public static final String LETTER_SUCCESSFULLY_SENT_PATTERN = "Letter '%s' successfully sent.";
    public static final String SENDING_LETTER_PATTERN = "Sending letter '%s'";
}
