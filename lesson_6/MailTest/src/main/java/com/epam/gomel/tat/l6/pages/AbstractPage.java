package com.epam.gomel.tat.l6.pages;

import com.epam.gomel.tat.l6.ui.Browser;

/**
 * Created by user on 20.03.15.
 */
public abstract class AbstractPage {
    public static final String HREF = "href";
    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }
}
