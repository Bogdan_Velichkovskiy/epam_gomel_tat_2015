package com.epam.gomel.tat.l6.service;

import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.exception.LetterValidationException;
import com.epam.gomel.tat.l6.pages.LetterContentPage;
import com.epam.gomel.tat.l6.pages.MailboxBasePage;
import com.epam.gomel.tat.l6.reporting.Logger;
import com.epam.gomel.tat.l6.utils.FileUtil;

import static com.epam.gomel.tat.l6.reporting.Constants.MailOperationMessagePatterns.*;

import java.io.IOException;

/**
 * Created by user on 20.03.15.
 */
public class MailGuiService {

    public void sendMail(MailLetter letter) {
        Logger.info(String.format(SENDING_LETTER_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage()
                .openComposeMailPage()
                .sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
        Logger.info(String.format(LETTER_SUCCESSFULLY_SENT_PATTERN, letter));
    }

    public void checkMailInInboxList(MailLetter letter) {
        Logger.info(String.format(CHECKING_LETTER_IN_INBOX_LIST_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        Logger.info(OPENING_LETTER_CONTENT_PAGE);
        LetterContentPage letterPage = mailbox.openInboxPage().openLetter(letter.getSubject());
        Logger.info(String.format(RETRIEVING_LETTER_FROM_LETTER_CONTENT_PAGE));
        MailLetter sentLetter = letterPage.getLetter();
        compareLetters(letter, sentLetter);
    }

    public void checkMailInSentList(MailLetter letter) {
        Logger.info(String.format(CHECKING_LETTER_IN_SENT_LIST_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        Logger.info(OPENING_LETTER_CONTENT_PAGE);
        LetterContentPage letterPage = mailbox.openSentPage().openLetter(letter.getSubject());
        Logger.info(String.format(RETRIEVING_LETTER_FROM_LETTER_CONTENT_PAGE));
        MailLetter sentLetter = letterPage.getLetter();
        compareLetters(letter, sentLetter);
    }

    public void checkMailInTrashList(MailLetter letter) {
        Logger.info(String.format(CHECKING_LETTER_IN_TRASH_LIST_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        Logger.info(OPENING_LETTER_CONTENT_PAGE);
        LetterContentPage letterPage = mailbox.openTrashPage().openLetter(letter.getSubject());
        Logger.info(String.format(RETRIEVING_LETTER_FROM_LETTER_CONTENT_PAGE));
        MailLetter sentLetter = letterPage.getLetter();
        compareLetters(letter, sentLetter);
    }

    public void checkMailInSpamList(MailLetter letter) {
        Logger.info(String.format(CHECKING_LETTER_IN_SPAM_LIST_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        Logger.info(OPENING_LETTER_CONTENT_PAGE);
        LetterContentPage letterPage = mailbox.openSpamPage().openLetter(letter.getSubject());
        Logger.info(String.format(RETRIEVING_LETTER_FROM_LETTER_CONTENT_PAGE));
        MailLetter sentLetter = letterPage.getLetter();
        compareLetters(letter, sentLetter);
    }

    public void deleteMailFromInbox(MailLetter letter) {
        Logger.info(String.format(DELETING_LETTER_FROM_INBOX_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().deleteLetter(letter.getSubject());
    }

    public void markMailAsSpam(MailLetter letter) {
        Logger.info(String.format(MARK_LETTER_AS_SPAM_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().markLetterAsSpam(letter.getSubject());
    }

    public void markMailAsNotSpam(MailLetter letter) {
        Logger.info(String.format(MARK_LETTER_AS_NOT_SPAM_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openSpamPage().markLetterAsNotSpam(letter.getSubject());
    }

    public boolean ifMailInSpamList(MailLetter letter) {
        Logger.info(String.format(CHECKING_FOR_LETTER_REMAINED_IN_SPAM_PATTERN, letter));
        MailboxBasePage mailbox = new MailboxBasePage().open();
        return !mailbox.openSpamPage().isMessageNotPresent(letter.getSubject());
    }

    private boolean compareLetters(MailLetter initialLetter, MailLetter sentLetter) {
        Logger.info(String.format(COMPARE_LETTER_CONTENT_PATTERN, initialLetter, sentLetter));
        try {
            if (!initialLetter.getReceiver().equals(sentLetter.getReceiver())) {
                throw new LetterValidationException(
                        String.format(ERROR_RECIEVERS_NOT_IDENTICAL, initialLetter.getReceiver(), sentLetter.getReceiver()));
            }
            if (!initialLetter.getSubject().equals(sentLetter.getSubject())) {
                throw new LetterValidationException(
                        String.format(ERROR_SUBJECTS_NOT_IDENTICAL, initialLetter.getSubject(), sentLetter.getSubject()));
            }
            if (!initialLetter.getContent().equals(sentLetter.getContent())) {
                throw new LetterValidationException(
                        String.format(ERROR_CONTENTS_NOT_IDENTICAL, initialLetter.getContent(), sentLetter.getContent()));
            }
            if (!FileUtil.compareFiles(initialLetter.getAttach(), sentLetter.getAttach())) {
                throw new LetterValidationException(
                        String.format(ERROR_FILE_CONTENTS_NOT_IDENTICAL, initialLetter.getAttach(), sentLetter.getAttach()));
            }
            Logger.info(COMPARISON_OF_LETTERS_SUCCESSFULLY_FINISHED);
            return true;
        } catch (LetterValidationException e) {
            Logger.error(e.getMessage(),e);
            return false;
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
            return false;
        }
    }
}
