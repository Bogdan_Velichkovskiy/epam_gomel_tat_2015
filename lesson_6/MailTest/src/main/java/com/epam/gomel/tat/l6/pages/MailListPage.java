package com.epam.gomel.tat.l6.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 22.03.15.
 */
public class MailListPage extends AbstractPage {

    protected static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    protected static final String MAIL_LINK_LOCATOR_PATTERN =
            "//div[@class='block-messages' and (@style='' or not(@style))]//a[.//span[@title='%s']]";
    protected static final String CHECKBOX_LETTER_LOCATOR_PATTERN = ".//input[@type='checkbox' and @value='%s']";
    protected static final By DELETE_BUTTON_LOCATOR = By.xpath(".//a[@title='Удалить (Delete)']");

    public LetterContentPage openLetter(String subject) {
        if (isMessagePresent(subject)) {
            browser.click(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
            browser.waitForAjaxProcessed();
            return new LetterContentPage();
        }
        return null;
    }

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

    public boolean isMessageNotPresent(String subject) {
        browser.waitForInvisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

    public void markLetter(String subject) {
        String href = browser.getElementAttribute(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)), HREF);
        final String MESSAGE_SUFFIX = "#message/";
        int startIndexMessageID = href.lastIndexOf(MESSAGE_SUFFIX) + MESSAGE_SUFFIX.length();
        String messageID = href.substring(startIndexMessageID);
        browser.click(By.xpath(String.format(CHECKBOX_LETTER_LOCATOR_PATTERN, messageID)));
    }

    public void deleteLetter(String subject) {
        markLetter(subject);
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
    }
}
