package com.epam.gomel.tat.l6.reporting.Constants;

/**
 * Created by user on 24.03.15.
 */
public class LoginOperationMessagePatterns {
    public static final String LOGIN_TO_ACCOUNT = "Login to account '%s'.";
    public static final String LOGIN_TO_ACCOUNT_WITH_WRONG_PASSWORD = "Login to account '%s' with wrong password.";
    public static final String LOGIN_SUCCESS_MESSAGE_PATTERN = "User '%s' successfully logged in.";
    public static final String LOGIN_SUCCESS_WRONG_PASSW_MESSAGE_PATTERN = "User '%s' successfully logged in with wrong password.";
    public static final String LOGIN_FAILED_MESSAGE_PATTERN = "Login failed. User Mail : '%s'";
    public static final String USER_LOGGED_OUT_PATTERN = "User '%s' logged out.";
}

