package com.epam.gomel.tat.l6.runner;

import com.epam.gomel.tat.l6.exception.NoParametersSpecifiedException;
import com.epam.gomel.tat.l6.reporting.CustomTestNgListener;
import com.epam.gomel.tat.l6.reporting.Logger;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

import static com.epam.gomel.tat.l6.reporting.Constants.CommonOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class Runner {

    public static void main(String[] args) {
        if (args.length == 0) {
            throw new NoParametersSpecifiedException(ERROR_NO_PARAMS);
        }
        Logger.info(ADDING_LISTENER_TO_TESTS);
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());

        Logger.info(ADDING_SUITES_TO_TESTS);
        List<String> suites = new ArrayList<String>();
        for (String arg : args) {
            suites.add(arg);
        }
        testNG.setTestSuites(suites);
        Logger.info(RUNNING_TESTS);
        testNG.run();
    }
}
