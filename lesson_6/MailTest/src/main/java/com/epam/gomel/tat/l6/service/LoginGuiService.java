package com.epam.gomel.tat.l6.service;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.exception.LoginFailedException;
import com.epam.gomel.tat.l6.exception.UnauthorizedAccessException;
import com.epam.gomel.tat.l6.pages.MailLoginPage;
import com.epam.gomel.tat.l6.pages.MailboxBasePage;
import com.epam.gomel.tat.l6.reporting.Logger;

import static com.epam.gomel.tat.l6.reporting.Constants.LoginOperationMessagePatterns.*;
/**
 * Created by user on 20.03.15.
 */
public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.info(String.format(LOGIN_TO_ACCOUNT, account.getLogin()));

        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());

        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.toLowerCase().equals(account.getEmail())) {
            Logger.fatal(String.format(LOGIN_FAILED_MESSAGE_PATTERN, account.getLogin()));
            throw new LoginFailedException(String.format(LOGIN_FAILED_MESSAGE_PATTERN, userEmail));
        }
        Logger.info(String.format(LOGIN_SUCCESS_MESSAGE_PATTERN, account.getLogin()));
    }

    public void unsuccessLoginToAccountMailbox(Account account) {
        Logger.info(String.format(LOGIN_TO_ACCOUNT_WITH_WRONG_PASSWORD, account.getLogin()));

        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());

        String userEmail = mailbox.getUserEmail();
        if (userEmail != null && userEmail.equals(account.getEmail())) {
            Logger.fatal(String.format(LOGIN_SUCCESS_WRONG_PASSW_MESSAGE_PATTERN, account.getLogin()));
            throw new UnauthorizedAccessException(String.format(LOGIN_SUCCESS_WRONG_PASSW_MESSAGE_PATTERN, userEmail));
        }
        Logger.info(String.format(LOGIN_FAILED_MESSAGE_PATTERN, account.getLogin()));
    }

    public void logout(Account account) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.logout();
        Logger.info(String.format(USER_LOGGED_OUT_PATTERN, account.getLogin()));
    }
}
