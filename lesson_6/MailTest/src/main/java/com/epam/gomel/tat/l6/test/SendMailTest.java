package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import com.epam.gomel.tat.l6.service.MailGuiService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.epam.gomel.tat.l6.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by user on 20.03.15.
 */
public class SendMailTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();
    private MailLetter letter = null;

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() throws FileNotFoundException {
        letter = generateAndGetLetter(defaultAccount.getEmail());
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in sent list", dependsOnMethods = "sendMail")
    public void checkMailInSentList() {
        mailGuiService.checkMailInSentList(letter);
    }

    @AfterClass(description = "Logout...")
    public void logout() {
        loginGuiService.logout(defaultAccount);
    }
}
