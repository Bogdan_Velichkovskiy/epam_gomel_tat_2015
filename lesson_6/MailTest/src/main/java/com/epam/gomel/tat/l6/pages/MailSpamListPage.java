package com.epam.gomel.tat.l6.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 21.03.15.
 */
public class MailSpamListPage extends MailListPage {

    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath(".//a[@title='Не спам!']");

    public MailSpamListPage markLetterAsNotSpam(String subject) {
        markLetter(subject);
        browser.click(NOT_SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return this;
    }
}

