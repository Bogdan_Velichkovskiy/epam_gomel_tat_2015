package com.epam.gomel.tat.l6.reporting;

import com.epam.gomel.tat.l6.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

import static com.epam.gomel.tat.l6.reporting.Constants.CommonOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class CustomTestNgListener implements IResultListener2 {

    @Override
    public void beforeConfiguration(ITestResult result) {
        String message = String.format(CONFIG_START, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        String message = String.format(CONFIG_SUCCESS, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        String message = String.format(CONFIG_FAILED, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.error(message, result.getThrowable());
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        String message = String.format(CONFIG_SKIPPED, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onTestStart(ITestResult result) {
        String message = String.format(TEST_METHOD_START, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        String message = String.format(TEST_METHOD_SUCCESS, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        String message = String.format(TEST_METHOD_FAILED, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.error(message, result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        String message = String.format(TEST_METHOD_SKIPPED, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.info(message);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        String message = String.format(TEST_METHOD_FAILED, result.getMethod().getInstance().getClass().getCanonicalName(),
                result.getMethod().getMethodName());
        Logger.error(message, result.getThrowable());
    }

    @Override
    public void onStart(ITestContext context) {
        String message = String.format(START, context.getCurrentXmlTest().getName());
        Logger.info(message);
    }

    @Override
    public void onFinish(ITestContext context) {
        String message = String.format(FINISH, context.getName());
        Browser.get().kill();
        Logger.info(message);
    }
}
