package com.epam.gomel.tat.l6.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class MailInboxListPage extends MailListPage {

    private static final By SPAM_BUTTON_LOCATOR = By.xpath(".//a[@title='Это спам! (Shift + s)']");

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxListPage markLetterAsSpam(String subject) {
        markLetter(subject);
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return this;
    }


}
