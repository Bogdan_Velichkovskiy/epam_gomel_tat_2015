package com.epam.gomel.tat.l6.exception;

/**
 * Created by user on 20.03.15.
 */
public class NoParametersSpecifiedException extends RuntimeException {
    public NoParametersSpecifiedException() {
        super();
    }

    public NoParametersSpecifiedException(String message) {
        super(message);
    }

    public NoParametersSpecifiedException(Throwable cause) {
        super(cause);
    }

    public NoParametersSpecifiedException(String message, Throwable cause) {
        super(message, cause);
    }
}
