package com.epam.gomel.tat.l6.exception;

/**
 * Created by user on 23.03.15.
 */
public class LetterValidationException extends Exception {
    public LetterValidationException() {
        super();
    }

    public LetterValidationException(String message) {
        super(message);
    }

    public LetterValidationException(Throwable cause) {
        super(cause);
    }

    public LetterValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
