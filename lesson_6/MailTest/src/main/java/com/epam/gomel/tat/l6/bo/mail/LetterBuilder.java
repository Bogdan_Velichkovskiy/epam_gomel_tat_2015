package com.epam.gomel.tat.l6.bo.mail;

import com.epam.gomel.tat.l6.utils.FileUtil;
import com.epam.gomel.tat.l6.utils.RandomUtils;

import java.io.FileNotFoundException;

/**
 * Created by user on 22.03.15.
 */
public class LetterBuilder {

    private static String FILE_DIRECTORY = "c:\\temp\\";

    public static MailLetter generateAndGetLetter(String receiver) throws FileNotFoundException {

        MailLetter letter = new MailLetter();
        letter.setReceiver(receiver);
        letter.setSubject(RandomUtils.getRandomString(10).toUpperCase());
        letter.setContent(RandomUtils.getRandomString(20));
        letter.setAttach(FileUtil.generateFileInDir(FILE_DIRECTORY));

        return letter;
    }
}
