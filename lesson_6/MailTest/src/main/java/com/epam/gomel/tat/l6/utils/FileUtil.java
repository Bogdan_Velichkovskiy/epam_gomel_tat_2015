package com.epam.gomel.tat.l6.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 18.03.15.
 */
public class FileUtil {
    public static boolean compareFiles(String filePath1, String filePath2) throws IOException {
        File file1 = new File(filePath1);
        File file2 = new File(filePath2);

        if (file1.length() != file2.length()) {
            return false;
        }

        List<String> firstList = null;
        List<String> secondList = null;
        firstList = getFileContent(filePath1);
        secondList = getFileContent(filePath2);

        for (int i = 0; i < firstList.size(); i++) {
            if (!firstList.get(i).equals(secondList.get(i))) {
                return false;
            }
        }
        return true;
    }

    private static List<String> getFileContent(String filePath) throws IOException {
        BufferedReader br = null;
        List<String> result = new ArrayList<String>();
        String line;

        br = new BufferedReader(new FileReader(filePath));
        while ((line = br.readLine()) != null) {
            result.add(line);
        }
        br.close();
        return result;
    }

    public static String generateFileInDir(String dir) throws FileNotFoundException {
        final String fileExt = ".txt";
        String filePath = dir + RandomUtils.getRandomString(10) + fileExt;
        PrintWriter pw = new PrintWriter(filePath);

        fillFileRandomContent(pw);
        if (pw !=null) {
            pw.close();
        }
        return filePath;
    }

    private static void fillFileRandomContent(PrintWriter pw) {
        for (int i = 1; i <= 10; i++) {
            pw.write(RandomUtils.getRandomString(i)+"\n");
        }
    }
}
