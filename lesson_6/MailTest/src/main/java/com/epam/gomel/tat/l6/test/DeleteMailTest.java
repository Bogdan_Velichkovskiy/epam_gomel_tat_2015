package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import com.epam.gomel.tat.l6.service.MailGuiService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.epam.gomel.tat.l6.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by user on 21.03.15.
 */
public class DeleteMailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = null;
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() throws FileNotFoundException {
        letter = generateAndGetLetter(defaultAccount.getEmail());
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Delete mail", dependsOnMethods = "sendMail")
    public void deleteMail() {
        mailGuiService.deleteMailFromInbox(letter);
    }

    @Test(description = "Check mail in trash list", dependsOnMethods = "deleteMail")
    public void checkMailInTrashList() {
        mailGuiService.checkMailInTrashList(letter);
    }

    @AfterClass(description = "Logout...")
    public void logout() {
        loginGuiService.logout(defaultAccount);
    }
}
