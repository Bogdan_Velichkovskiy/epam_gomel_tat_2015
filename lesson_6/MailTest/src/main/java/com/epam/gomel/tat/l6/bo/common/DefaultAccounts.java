package com.epam.gomel.tat.l6.bo.common;

/**
 * Created by user on 20.03.15.
 */
public enum DefaultAccounts {

    DEFAULT_MAIL("testlogintat2015", "test12345", "testlogintat2015@yandex.ru"),
    DEFAULT_MAIL_WRONG_PASSWORD("testlogintat2015", "_test12345", "testlogintat2015@yandex.ru");

    private Account account;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}
