package com.epam.gomel.tat.l6.reporting.Constants;

/**
 * Created by user on 24.03.15.
 */
public class BrowserOperationsMessagePatterns {
    public static final String CLICK_PATTERN = "Click by '%s'.";
    public static final String WAITING_FOR_INVISIBLE_PATTERN = "Waiting for invisible by '%s'.";
    public static final String INITIALIZING_BROWSER = "Initializing browser.";
    public static final String CONFIGURING_PROFILE = "Configuring profile.";
    public static final String OPENING_URL_PATTERN = "Opening '%s' url.";
    public static final String CLOSING_BROWSER = "Closing browser.";
    public static final String NAVIGATE_PATTERN = "Navigate to '%s'.";
    public static final String TYPE_TEXT_PATTERN = "Type text '%s' by '%s'.";
    public static final String GETTING_TEXT_PATTERN = "Getting text by '%s'.";
    public static final String GETTING_ATTRIBUTE_PATTERN = "Getting attribute '%s' by '%s'.";
    public static final String LOCATING_FOR_ELEMENTS_PATTERN = "Locating for elements by '%s'.";
    public static final String WAITING_FOR_PRESENT_PATTERN = "Waiting for present by '%s'.";
    public static final String WAITING_FOR_VISIBLE_PATTERN = "Waiting for visible by '%s'.";
    public static final String CHECKING_FOR_VISIBLE_PATTERN = "Checking for visible by '%s'.";
    public static final String PROCESSING_AJAX_WITH_TIMEOUT_PATTERN = "Processing AJAX with timeout '%s'.";
    public static final String WAITING_FOR_DOWNLOADING_PATTERN = "Waiting for downloading '%s'.";
    public static final String SUBMIT_FORM_PATTERN = "Submit form by '%s'.";
}
