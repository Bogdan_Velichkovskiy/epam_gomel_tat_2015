package com.epam.gomel.tat.lesson_6.pages;

import com.epam.gomel.tat.lesson_6.ui.Browser;

public abstract class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }

}
