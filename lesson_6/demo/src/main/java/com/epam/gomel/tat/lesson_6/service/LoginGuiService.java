package com.epam.gomel.tat.lesson_6.service;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_6.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_6.reporting.Logger;

public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.log.info("Login to account " + account.getLogin());
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null && !userEmail.equals(account.getEmail())) {
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void checkLoginImpossible(Account account) {
        // TODO
    }
}
