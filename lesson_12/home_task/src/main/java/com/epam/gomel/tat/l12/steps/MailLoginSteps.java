package com.epam.gomel.tat.l12.steps;

import com.epam.gomel.tat.l12.bo.common.Account;
import com.epam.gomel.tat.l12.bo.common.AccountFactory;
import com.epam.gomel.tat.l12.service.LoginService;
import junit.framework.Assert;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Bahdan_Velichkouski on 4/10/2015.
 */
public class MailLoginSteps {
    protected LoginService loginService = new LoginService();
    protected Account user;

    @Given("Actor is Mailbox Owner")
    public void actorIsMailBoxOwner() {
        user = AccountFactory.createUser();
    }

    @Given("Actor has correct login and password")
    public void actorHasCorrectLoginAndPassword() {
        AccountFactory.build(user).withCorrectLogin().withCorrectPassword().withCorrectEmail();
    }

    @Given("Actor has access to mailbox")
    public void checkAccessToMailBox() {
        Assert.assertTrue(loginService.isMailListAccessible(user));
    }

    @When("Actor login to mailbox")
    public void actorLoginToMailbox() {
        loginService.doLogin(user);
    }

    @Then("Actor has access to mail list")
    public void checkAccessToMailList() {
        Assert.assertTrue(loginService.isMailListAccessible(user));
    }
}
