package com.epam.gomel.tat.l12.steps;

import junit.framework.Assert;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class MarkSpamMailSteps extends SendMailSteps {
    @When("Actor mark mail as spam")
    public void markAsSpamMail() {
        mailService.markMailAsSpam();
    }

    @Then("Spam mail should be in spam list")
    public void checkMessageInSpamList() {
        Assert.assertTrue(mailService.isMailInSpamList());
    }
}
