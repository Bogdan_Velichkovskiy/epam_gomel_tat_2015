package com.epam.gomel.tat.l12.service;

import com.epam.gomel.tat.l12.bo.common.Account;
import com.epam.gomel.tat.l12.bo.mail.MailLetter;

import static com.epam.gomel.tat.l12.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by Bahdan_Velichkouski on 4/10/2015.
 */
public class MailService {
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = null;

    public void sendMail(Account account) {
        letter = generateAndGetLetter(account.getEmail());
        mailGuiService.sendMail(letter);
    }

    public boolean isMailInSentList() {
        return mailGuiService.isMailInSentList(letter);
    }

    public void deleteMail() {
        mailGuiService.deleteMailFromInbox(letter);
    }

    public boolean isMailInTrashList() {
        return mailGuiService.isMailInTrashList(letter);
    }

    public void markMailAsSpam() {
        mailGuiService.markMailAsSpam(letter);
    }

    public boolean isMailInSpamList() {
        return mailGuiService.isMailInSpamList(letter);
    }

    public void markMailAsNotSpam() {
        mailGuiService.markMailAsNotSpam(letter);
    }

    public boolean isMailInInboxList() {
        return mailGuiService.isMailInInboxList(letter);
    }
}
