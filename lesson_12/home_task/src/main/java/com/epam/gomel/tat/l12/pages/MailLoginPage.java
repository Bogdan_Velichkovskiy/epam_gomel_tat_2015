package com.epam.gomel.tat.l12.pages;

import com.epam.gomel.tat.l12.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class MailLoginPage extends AbstractPage {

    public static final String BASE_URL = "http://mail.yandex.ru";

    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public MailLoginPage open() {
        Browser.get().open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        Browser browser = Browser.get();
        browser.clearElementText(LOGIN_INPUT_LOCATOR);
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.submit(PASSWORD_INPUT_LOCATOR);
        return new MailboxBasePage();
    }
}
