package com.epam.gomel.tat.l12.steps;

import com.epam.gomel.tat.l12.service.MailService;
import junit.framework.Assert;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class SendMailSteps extends MailLoginSteps {
    protected MailService mailService = new MailService();

    @When("Actor send mail")
    public void checkSendMail() {
        mailService.sendMail(user);
    }

    @Then("Sent message should be in sent list")
    public void checkMessageInSentList() {
        Assert.assertTrue(mailService.isMailInSentList());
    }
}
