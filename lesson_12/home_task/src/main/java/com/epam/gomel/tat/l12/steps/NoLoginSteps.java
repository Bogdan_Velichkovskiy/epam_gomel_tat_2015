package com.epam.gomel.tat.l12.steps;

import com.epam.gomel.tat.l12.bo.common.AccountFactory;
import junit.framework.Assert;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class NoLoginSteps extends MailLoginSteps {

    @Given("Actor has correct login and incorrect password")
    public void actorHasCorrectLoginAndIncorrectPassword() {
        AccountFactory.build(user).withCorrectLogin().withIncorrectPassword().withCorrectEmail();
    }

    @Then("Actor has no access to mail list")
    public void checkNoAccessToMailList() {
        Assert.assertFalse(loginService.isMailListAccessible(user));
    }
}
