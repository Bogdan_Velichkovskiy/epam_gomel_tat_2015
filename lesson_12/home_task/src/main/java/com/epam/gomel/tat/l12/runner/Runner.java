package com.epam.gomel.tat.l12.runner;

import com.epam.gomel.tat.l12.config.GlobalConfig;
import com.epam.gomel.tat.l12.reporting.Logger;
import com.epam.gomel.tat.l12.steps.MailLoginSteps;
import com.epam.gomel.tat.l12.steps.NoLoginSteps;
import org.jbehave.core.embedder.Embedder;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.util.Arrays;
import java.util.List;

import static com.epam.gomel.tat.l12.reporting.Constants.CommonOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class Runner {
    private static GlobalConfig config = null;
    private static Embedder embedder = new Embedder();
    private static List<String> storyPaths = Arrays
            .asList("stories/uncuccess_login_to_mailbox.story");

    public static void main(String[] args) {
        config = GlobalConfig.getInstance();
        new Runner().run(args);
    }

    private void run(String[] args) {

        CmdLineParser parser = new CmdLineParser(config);

        try {
            Logger.info(PARSING_ARGUMENTS);
            parser.parseArgument(args);
            Logger.info(RUNNING_TESTS);
            //embedder.candidateSteps().add(new MailLoginSteps());
            //embedder.candidateSteps().add(new NoLoginSteps());
            //embedder.runStoriesAsPaths(storyPaths);
        } catch (CmdLineException e) {
            Logger.error(e.getMessage(),e);
            parser.printUsage(System.err);
        } catch (Exception e) {
            Logger.error(e.getMessage(),e);
        }
    }

}
