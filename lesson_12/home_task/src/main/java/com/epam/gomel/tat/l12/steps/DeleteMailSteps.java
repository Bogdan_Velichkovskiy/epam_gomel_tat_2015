package com.epam.gomel.tat.l12.steps;

import junit.framework.Assert;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class DeleteMailSteps extends SendMailSteps {
    @When("Actor delete mail")
    public void deleteMail() {
        mailService.deleteMail();
    }

    @Then("Deleted mail should be in trash list")
    public void checkMessageInTrashList() {
        Assert.assertTrue(mailService.isMailInTrashList());
    }
}
