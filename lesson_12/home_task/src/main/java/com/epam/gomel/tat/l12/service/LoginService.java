package com.epam.gomel.tat.l12.service;

import com.epam.gomel.tat.l12.bo.common.Account;

/**
 * Created by Bahdan_Velichkouski on 4/10/2015.
 */
public class LoginService {
    private LoginGuiService loginGuiService = new LoginGuiService();

    public void doLogin(Account user) {
        loginGuiService.loginToAccountMailbox(user);
    }

    public boolean isMailListAccessible(Account user) {
        return loginGuiService.isMailListAccessible();
    }
}
