package com.epam.gomel.tat.l12.bo.common;

/**
 * Created by user on 20.03.15.
 */
public enum DefaultAccounts {

    DEFAULT_ACCOUNT("testlogintat2015", "test12345", "testlogintat2015@yandex.ru"),
    DEFAULT_ACCOUNT_WRONG_PASSWORD("testlogintat2015", "_test12345", "testlogintat2015@yandex.ru");

    private Account account;
    private String login;
    private String password;
    private String email;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Account getAccount() {
        return account;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
