package com.epam.gomel.tat.l12.ui;

import com.epam.gomel.tat.l12.config.GlobalConfig;
import com.epam.gomel.tat.l12.reporting.Logger;
import com.epam.gomel.tat.l12.utils.FileUtil;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.epam.gomel.tat.l12.reporting.Constants.BrowserOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class Browser {

    public static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 30;
    public static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 20;
    public static final int AJAX_TIMEOUT = 20;
    public static final int WAIT_ELEMENT_TIMEOUT = 30;
    public static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, "
            + "application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, "
            + "application/octet-stream, application/txt";
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";

    private WebDriver driver;
    private boolean augmented;

    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = null;
        synchronized (Browser.class) {
            instance = instances.get(currentThread);
            if (instance != null) {
                return instance;
            }
            instance = init();
            instances.put(currentThread, instance);
        }
        return instance;
    }

    private static Browser init() {
        Logger.info(INITIALIZING_BROWSER);
        BrowserType browserType = GlobalConfig.getInstance().getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case FIREFOX:
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case REMOTE:
                try {
                    String connectTo = String.format("http://%s:%s/wd/hub", GlobalConfig.getInstance().getHost(),
                            GlobalConfig.getInstance().getPort());
                    DesiredCapabilities capability = DesiredCapabilities.firefox();
                    capability.setCapability(FirefoxDriver.PROFILE, getFireFoxProfile());
                    driver = new RemoteWebDriver(new URL(connectTo), capability);
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", FileUtil.getCanonicalPathToResourceFile("chromedriver.exe"));
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", GlobalConfig.getInstance().getDownloadDirPath());
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                cap.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new ChromeDriver(cap);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case HTMLUNIT:
                DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
                driver = new HtmlUnitDriver(capabilities);
                ((HtmlUnitDriver)driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
        }
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        Logger.info(CONFIGURING_PROFILE);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalConfig.getInstance().getDownloadDirPath());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String url) {
        Logger.info(String.format(OPENING_URL_PATTERN, url));
        driver.get(url);
    }

    public static void kill() {
        Logger.info(CLOSING_BROWSER);
        Thread currentThread = Thread.currentThread();
        synchronized (Browser.class) {
            Browser instance = instances.get(currentThread);
            if (instance != null) {
                try {
                    instance.driver.quit();
                } catch (Exception e) {
                    Logger.error("Cannot kill browser", e);
                } finally {
                    instances.remove(currentThread);
                }
            }
        }
    }

    public void configImplicitWait(int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public void takeScreenshot() {
        if (GlobalConfig.getInstance().getBrowserType() == BrowserType.REMOTE &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public void mouseOver(By locator) {
        Logger.info(String.format("Mouse over '%s'", locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        new Actions(driver).moveToElement(element).build().perform();
    }

    public void doubleClick(By locator) {
        Logger.info(String.format(DOUBLE_CLICK_PATTERN, locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        new Actions(driver).doubleClick(element).build().perform();
    }

    public void singleClick(By locator) {
        Logger.info(String.format(CLICK_PATTERN, locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        new Actions(driver).click(element).build().perform();
    }

    public void click(By locator) {
        Logger.info(String.format(CLICK_PATTERN, locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.click();
    }

    public void clickIfFound(By locator) {
        if (isPresent(locator)) {
            click(locator);
        }
    }

    public void dragAndDrop(By locator, int xOffset, int yOffset) {
        Logger.info(String.format("Dragging '%s' to (%d, %d)", locator, xOffset, yOffset));
        WebElement draggable = driver.findElement(locator);
        elementHighlight(draggable);
        new Actions(driver).dragAndDropBy(draggable, xOffset, yOffset).build().perform();
    }

    public void dragAndDrop(By source, By destination) {
        Logger.info(String.format("Dragging '%s' to '%s'", source, destination));
        WebElement draggable = driver.findElement(source);
        elementHighlight(draggable);
        new Actions(driver).dragAndDrop(draggable, driver.findElement(destination)).build().perform();
    }

    public void goTo(String href) {
        Logger.info(String.format(NAVIGATE_PATTERN, href));
        driver.navigate().to(href);
    }

    public void type(By locator, String text) {
        Logger.info(String.format(TYPE_TEXT_PATTERN, text, locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
    }

    public void clearElementText(By locator) {
        driver.findElement(locator).clear();
    }

    public String getElementText(By locator) {
        Logger.info(String.format(GETTING_TEXT_PATTERN, locator));
        return isPresent(locator) ? driver.findElement(locator).getText() : null;
    }

    public String getElementAttribute(By locator, String attribute) {
        Logger.info(String.format(GETTING_ATTRIBUTE_PATTERN, attribute, locator));
        return driver.findElement(locator).getAttribute(attribute);
    }

    public boolean isPresent(By locator) {
        Logger.info(String.format(LOCATING_FOR_ELEMENTS_PATTERN, locator));
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info(String.format(WAITING_FOR_PRESENT_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForTextPresentOnElement(final By locator, String textToAppear) {
        Logger.info(String.format(WAITING_FOR_PRESENT_PATTERN, locator));
        WebDriverWait wait = new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT);
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(locator), textToAppear));
    }

    public void waitForVisible(By locator) {
        Logger.info(String.format(WAITING_FOR_VISIBLE_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public boolean isElementVisible(By locator) {
        Logger.info(String.format(CHECKING_FOR_VISIBLE_PATTERN, locator));
        if (isPresent(locator)) {
            WebElement element = driver.findElement(locator);
            return element.isDisplayed();
        }
        return false;
    }

    public void waitForInvisible(By locator) {
        Logger.info(String.format(WAITING_FOR_INVISIBLE_PATTERN, locator));
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info(String.format(PROCESSING_AJAX_WITH_TIMEOUT_PATTERN, AJAX_TIMEOUT));
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void waitForFileDownload(String filePath) {
        Logger.info(String.format(WAITING_FOR_DOWNLOADING_PATTERN, filePath));
        File downloadedFile = new File(filePath);
        FluentWait wait = new FluentWait<File>(downloadedFile).withTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Nullable
            @Override
            public Boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public void submit(By locator) {
        Logger.info(String.format(SUBMIT_FORM_PATTERN, locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.submit();
    }

    public String getTitle() {
        return driver.getTitle();
    }
}