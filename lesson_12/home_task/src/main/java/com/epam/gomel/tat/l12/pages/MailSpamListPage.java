package com.epam.gomel.tat.l12.pages;

import com.epam.gomel.tat.l12.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by user on 21.03.15.
 */
public class MailSpamListPage extends MailListPage {

    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath(".//a[@data-action='notspam']");

    public MailSpamListPage markLetterAsNotSpam(String subject) {
        markLetter(subject);
        Browser browser = Browser.get();
        browser.click(NOT_SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return this;
    }
}

