package com.epam.gomel.tat.l12.pages;

import com.epam.gomel.tat.l12.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class MailboxBasePage extends AbstractPage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    private static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final By PROFILE_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    private static final By LOGOUT_LINK_LOCATOR = By.xpath("//a[contains(@href,'retpath=http%3A%2F%2Fwww.yandex.by')]");
    private static final By PROFILE_LINK_LOCATOR1 = By.xpath("//a[@id='nb-1']");
    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By LIST_TITLE_LOCATOR = By.xpath("//span[@class='b-messages-head__title']");

    public MailboxBasePage open() {
        Browser browser = Browser.get();
        browser.open(MailLoginPage.BASE_URL);
        return new MailboxBasePage();
    }

    public ComposeMailPage openComposeMailPage() {
        Browser.get().click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxListPage openInboxPage() {
        Browser browser = Browser.get();
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        Browser browser = Browser.get();
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        Browser browser = Browser.get();
        browser.click(TRASH_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        Browser browser = Browser.get();
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public String getUserEmail() {
        return Browser.get().getElementText(PROFILE_LINK_LOCATOR);
    }

    public void logout() {
        Browser browser = Browser.get();
        browser.click(PROFILE_LINK_LOCATOR1);
        browser.click(LOGOUT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
    }

    public boolean isMailListAccessible() {
        return Browser.get().isElementVisible(COMPOSE_BUTTON_LOCATOR);
    }
}
