package com.epam.gomel.tat.l12.steps;

import junit.framework.Assert;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class MarkAsNotSpamMailSteps extends MarkSpamMailSteps {
    @When("Actor mark mail as not spam")
    public void checkMarkAsNotSpamMail() {
        mailService.markMailAsNotSpam();
    }

    @Then("Not spam mail should be in inbox list")
    public void checkMessageInInboxList() {
        Assert.assertTrue(mailService.isMailInInboxList());
    }
}
