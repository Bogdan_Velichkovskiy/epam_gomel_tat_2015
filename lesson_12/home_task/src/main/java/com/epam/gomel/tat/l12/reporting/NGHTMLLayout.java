package com.epam.gomel.tat.l12.reporting;

import org.apache.log4j.HTMLLayout;
import org.apache.log4j.spi.LoggingEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 24.03.15.
 */
public class NGHTMLLayout extends HTMLLayout {

    private static final String rxTimestamp = "\\s*<\\s*tr\\s*>\\s*<\\s*td\\s*>\\s*(\\d*)\\s*<\\s*/td\\s*>";
    private String timeStampFormat = "yyyy-MM-dd HH:mm:ss";
    private DateFormat sdf = new SimpleDateFormat(timeStampFormat);

    public NGHTMLLayout()
    {
        super();
    }

    @Override
    public String format(LoggingEvent event) {
        String record = super.format(event);
        Pattern pattern = Pattern.compile(rxTimestamp);
        Matcher matcher = pattern.matcher(record);
        if (!matcher.find())
        {
            return record;
        }

        StringBuffer buffer = new StringBuffer(record);
        buffer.replace(matcher.start(1), matcher.end(1), sdf.format(new Date(event.getTimeStamp())));

        return buffer.toString();
    }

    public void setTimeStampFormat(String format) {
        this.timeStampFormat = format;
        this.sdf = new SimpleDateFormat(format);
        this.sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
    }

    public String getTimeStampFormat()
    {
        return this.timeStampFormat;
    }
}
