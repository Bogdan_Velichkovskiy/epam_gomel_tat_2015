package com.epam.gomel.tat.l12.pages;

import com.epam.gomel.tat.l12.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class MailInboxListPage extends MailListPage {

    private static final By SPAM_BUTTON_LOCATOR = By.xpath(".//a[@data-action='tospam']");

    public MailInboxListPage markLetterAsSpam(String subject) {
        markLetter(subject);
        Browser browser = Browser.get();
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return this;
    }


}
