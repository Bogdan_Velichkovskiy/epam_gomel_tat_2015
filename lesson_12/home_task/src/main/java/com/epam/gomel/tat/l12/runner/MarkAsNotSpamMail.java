package com.epam.gomel.tat.l12.runner;

import com.epam.gomel.tat.l12.steps.MarkAsNotSpamMailSteps;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class MarkAsNotSpamMail extends AbstractJUnitStories {
    private static final String MARK_AS_NOT_SPAM_MAIL_STORY = "mark_as_not_spam_mail.story";

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new MarkAsNotSpamMailSteps());
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()).getFile(),
                Arrays.asList("**/" + MARK_AS_NOT_SPAM_MAIL_STORY), null);
    }
}
