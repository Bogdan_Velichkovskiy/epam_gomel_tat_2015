package com.epam.gomel.tat.l12.service;

import com.epam.gomel.tat.l12.bo.common.Account;
import com.epam.gomel.tat.l12.exception.LoginFailedException;
import com.epam.gomel.tat.l12.exception.UnauthorizedAccessException;
import com.epam.gomel.tat.l12.pages.MailLoginPage;
import com.epam.gomel.tat.l12.pages.MailboxBasePage;
import com.epam.gomel.tat.l12.reporting.Logger;

import static com.epam.gomel.tat.l12.reporting.Constants.LoginOperationMessagePatterns.*;
/**
 * Created by user on 20.03.15.
 */
public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.info(String.format(LOGIN_TO_ACCOUNT, account.getLogin()));
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
    }

    public void logout(Account account) {
        Logger.info(String.format(USER_LOGOUT_PATTERN, account.getLogin()));
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.logout();
    }

    public boolean isMailListAccessible() {
        return new MailboxBasePage().open().isMailListAccessible();
    }
}
