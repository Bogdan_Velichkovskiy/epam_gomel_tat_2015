package com.epam.gomel.tat.l12.bo.common;

/**
 * Created by Bahdan_Velichkouski on 4/10/2015.
 */
public class AccountFactory {
    public static Account createUser() {
        return new Account();
    }

    public static Builder build(Account user) {
        return new Builder(user);
    }

    public static class Builder {
        Account accountToBuild;

        public Builder(Account user) {
            accountToBuild = user;
        }

        public Builder withCorrectLogin() {
            accountToBuild.setLogin(DefaultAccounts.DEFAULT_ACCOUNT.getLogin());
            return this;
        }

        public Builder withCorrectPassword() {
            accountToBuild.setPassword(DefaultAccounts.DEFAULT_ACCOUNT.getPassword());
            return this;
        }

        public Builder withIncorrectPassword() {
            accountToBuild.setPassword(DefaultAccounts.DEFAULT_ACCOUNT_WRONG_PASSWORD.getPassword());
            return this;
        }

        public Builder withCorrectEmail() {
            accountToBuild.setEmail(DefaultAccounts.DEFAULT_ACCOUNT.getEmail());
            return this;
        }

    }
}
