package com.epam.gomel.tat.l12.runner;

import com.epam.gomel.tat.l12.steps.NoLoginSteps;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Bahdan_Velichkouski on 4/13/2015.
 */
public class NoLoginToMailbox extends AbstractJUnitStories {
    private static final String LOGIN_STORY = "uncuccess_login_to_mailbox.story";

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new NoLoginSteps());
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()).getFile(),
                Arrays.asList("**/" + LOGIN_STORY), null);
    }
}
