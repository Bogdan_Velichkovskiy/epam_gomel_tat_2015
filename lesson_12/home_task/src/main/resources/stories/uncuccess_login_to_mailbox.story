Feature : Unsuccess login to mailbox


Meta:
@login

Narrative:
As a Mailbox User
I want to login to my mailbox
So that I have access to my mail list

Scenario: Unsuccess login as mailbox owner
Given Actor is Mailbox Owner
And Actor has correct login and incorrect password
When Actor login to mailbox
Then Actor has no access to mail list