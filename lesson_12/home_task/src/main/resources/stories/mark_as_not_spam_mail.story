Feature : mark mail as not spam

Meta:
@mail

Narrative:
As a Mailbox user
I want to login to my mailbox
So that I can mark mail as not spam

Background:
Scenario: Succes login as mailbox owner
Given Actor is Mailbox Owner
And Actor has correct login and password
When Actor login to mailbox
Then Actor has access to mail list

Scenario: Succes mark mail as not spam
Given Actor has access to mailbox
When Actor send mail
And Actor mark mail as spam
And Actor mark mail as not spam
Then Not spam mail should be in inbox list