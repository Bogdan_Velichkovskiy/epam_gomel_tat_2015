Feature : send mail

Meta:
@mail

Narrative:
As a Mailbox user
I want to login to my mailbox
So that I can send mail

Background:
Scenario: Succes login as mailbox owner
Given Actor is Mailbox Owner
And Actor has correct login and password
When Actor login to mailbox
Then Actor has access to mail list

Scenario: Succes send mail
Given Actor has access to mailbox
When Actor send mail
Then Sent message should be in sent list