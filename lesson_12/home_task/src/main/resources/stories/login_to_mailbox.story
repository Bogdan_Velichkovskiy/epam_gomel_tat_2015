Feature : Login to mailbox

Meta:
@login

Narrative:
As a Mailbox User
I want to login to my mailbox
So that I have access to my mail list

Scenario: Succes login as mailbox owner
Given Actor is Mailbox Owner
And Actor has correct login and password
When Actor login to mailbox
Then Actor has access to mail list