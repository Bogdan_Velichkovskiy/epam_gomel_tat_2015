package com.epam.tat.lesson9.page;

import com.epam.tat.lesson9.browser.Browser;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class GooglePage {

    private static final String GOOGLE_HOME = "http://google.ru" ;

    public static GooglePage open() {
        Browser.get().open(GOOGLE_HOME);
        return new GooglePage();
    }
}
