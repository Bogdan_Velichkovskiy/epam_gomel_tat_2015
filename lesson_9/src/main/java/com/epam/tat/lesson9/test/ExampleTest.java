package com.epam.tat.lesson9.test;

import com.epam.tat.lesson9.page.GooglePage;
import com.epam.tat.lesson9.page.PriorHomePage;
import com.epam.tat.lesson9.utils.Logger;
import com.epam.tat.lesson9.utils.Timeout;
import org.testng.annotations.*;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
//@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
//        MethodListener.class, MySuiteListener.class})
//    @Listeners({MyTestListener.class})
public class ExampleTest extends ParallelTest {

    @Test(description = "Try to open google page", groups = {"grp1"})
    @Parameters(value = "number")
    public void openPage(@Optional(value = "0") String number) {
        Logger.debug("This test have try: " + number);
        GooglePage.open();
        Timeout.sleep(5);
    }

    @BeforeGroups(groups = {"grp2", "grp1"})
    public void beforeGrp() {
        Logger.info("before group");
    }

    @Test(description = "Perform login to Prior", groups = {"grp2"})
    public void performLoginToPrior() {
        PriorHomePage.open().login("user", "1q2w3e").checkAntiRobotWorks();
        Timeout.sleep(5);
    }
}
