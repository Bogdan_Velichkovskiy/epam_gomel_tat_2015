package com.epam.tat.lesson9.test.plunker;

import com.epam.tat.lesson9.browser.Browser;
import com.epam.tat.lesson9.test.BaseTest;
import com.epam.tat.lesson9.utils.Timeout;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class PlunkerWindowsAndFramesTest extends BaseTest {

    private static final String PLNKR = "http://run.plnkr.co/plunks/VUOWcZ7iZbd7UG2yDS1A/";

    @Test
    public void downloadFile() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.id("download-link"));
        Browser.get().click(By.id("download-link"));
        Timeout.sleep(10);
    }

    @Test
    public void checkElementInFrameFailed() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.xpath("//span[contains(text(),'Users')]"));

    }

    @Test
    public void checkElementInFrameSuccess() {
        Browser.get().open(PLNKR);
//        Browser.get().switchToFrame(By.xpath("//iframe"));
        Browser.get().switchToFrame("custom-frame");
        Browser.get().waitForPresent(By.xpath("//span[contains(text(),'Users')]"));
    }

    @Test
    public void openNewWindow() {
        Browser.get().open(PLNKR);
        Browser.get().openNewWindowOnClick(By.id("window-link"));
        Browser.get().getAllWindowNames();
        Browser.get().waitForPresent(By.xpath("//button[@type='submit']"));
    }

    @Test
    public void openNewPopUpAndSwitchBack() {
        Browser.get().open(PLNKR);
        String oldIdHandler = Browser.get().openNewWindowOnClick(By.id("popup-link"));
        Browser.get().waitForPresent(By.xpath("//h3[text()='HTML/CSS']"));
        Browser.get().switchToWindow(oldIdHandler);
        Browser.get().waitForPresent(By.id("popup-link"));
    }
}
