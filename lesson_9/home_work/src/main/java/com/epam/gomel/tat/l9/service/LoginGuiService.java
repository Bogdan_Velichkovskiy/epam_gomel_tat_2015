package com.epam.gomel.tat.l9.service;

import com.epam.gomel.tat.l9.bo.common.Account;
import com.epam.gomel.tat.l9.exception.LoginFailedException;
import com.epam.gomel.tat.l9.pages.LoginPage;
import com.epam.gomel.tat.l9.pages.PassportPage;
import com.epam.gomel.tat.l9.reporting.Logger;

import static com.epam.gomel.tat.l9.reporting.Constants.LoginOperationMessagePatterns.*;
/**
 * Created by user on 20.03.15.
 */
public class LoginGuiService {

    public void loginToAccount(Account account) {
        Logger.info(String.format(LOGIN_TO_ACCOUNT, account.getLogin()));
        PassportPage passport = new LoginPage().open().login(account.getLogin(), account.getPassword());

        String userLogin = passport.getUserLogin();
        if (userLogin == null || !userLogin.equalsIgnoreCase(account.getLogin())) {
            Logger.fatal(String.format(LOGIN_FAILED_MESSAGE_PATTERN, account.getLogin()));
            throw new LoginFailedException(String.format(LOGIN_FAILED_MESSAGE_PATTERN, userLogin));
        }
    }
}
