package com.epam.gomel.tat.l9.pages;

import com.epam.gomel.tat.l9.config.GlobalConfig;
import com.epam.gomel.tat.l9.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Bahdan_Velichkouski on 4/6/2015.
 */
public class YandexDiskPage extends AbstractPage {
    private static final By CLOSE_POPUP_DIALOG_BUTTON_LOCATOR = By.xpath("//a[@class='_nb-popup-close js-dialog-close']");
    private static final By CLOSE_COMPLETED_DIALOG_BUTTON_LOCATOR = By.xpath("//a[@class='_nb-popup-close ns-action js-cross']");
    private static final By DROP_ZONE_LOCATOR = By.xpath("//div[@class='ns-view-boxDropzone ns-view-visible']");
    private static final By UPLOAD_FILE_BUTTON_LOCATOR = By.xpath("//input[@class='_nb-file-intruder-input']");
    private static final By DOWNLOAD_DONE_ICON_LOCATOR = By.xpath("//div[@class='b-item-upload__icon b-item-upload__icon_done']");
    private static final By TRASH_LOCATOR = By.xpath("//div[contains(@data-params, 'trash')]");
    private static final By NOTIFY_LOCATOR = By.xpath("//div[@class='ns-view-notifications notifications ns-view-visible']");
    private static final String PROGRESS_BAR_LOCATOR_PATTERN = "//div[@class='b-item-upload__label b-item-upload__label_smart' and @title='%s']";
    private static final String FILE_LINK_LOCATOR_PATTERN = "//div[@title='%s']";
    private static final String FILE_CHECKBOX_LOCATOR_PATTERN = "(//div[@title='%s']//input[last()])[last()]";
    private static final String FILE_ICON_LOCATOR_PATTERN = "//div[contains(@data-params, '%s')]//div[@class='nb-icon-resource__image nb-icon-resource__image_preview nb-icon-resource__image_document']";
    private static final String FILE_DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//button[@data-click-action='resource.download' and contains(@data-params, '%s')]";

    public YandexDiskPage open() {
        browser.open("https://disk.yandex.ru/client/disk");
        return this;
    }

    public void closePopup() {
        browser.configImplicitWait(2);
        browser.clickIfFound(CLOSE_POPUP_DIALOG_BUTTON_LOCATOR);
        browser.takeScreenshot();
        browser.configImplicitWait(Browser.WAIT_ELEMENT_TIMEOUT);
    }

    public void uploadFile(String filePath) {
        attachFile(UPLOAD_FILE_BUTTON_LOCATOR, filePath);
        browser.waitForPresent(DOWNLOAD_DONE_ICON_LOCATOR);
        browser.takeScreenshot();
        browser.click(CLOSE_COMPLETED_DIALOG_BUTTON_LOCATOR);
        browser.takeScreenshot();
    }

    private void attachFile(By locator, String text) {
        browser.type(locator, text);
        browser.takeScreenshot();
    }

    public void downloadFile(String fileName) {
        browser.mouseOver(By.xpath(String.format(FILE_LINK_LOCATOR_PATTERN, fileName)));
        browser.takeScreenshot();
        browser.click(By.xpath(String.format(FILE_ICON_LOCATOR_PATTERN, fileName)));
        browser.takeScreenshot();
        browser.click(By.xpath(String.format(FILE_DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName)));
        browser.takeScreenshot();
        browser.waitForFileDownload(GlobalConfig.getInstance().getDownloadDirPath() + fileName);
    }

    public void moveFileToTrash(String fileName) {
        browser.dragAndDrop(By.xpath(String.format(FILE_ICON_LOCATOR_PATTERN, fileName)), TRASH_LOCATOR);
        browser.takeScreenshot();
        browser.waitForVisible(NOTIFY_LOCATOR);
        browser.takeScreenshot();
    }

    public TrashPage openTrashPage() {
        browser.doubleClick(TRASH_LOCATOR);
        return new TrashPage();
    }

}
