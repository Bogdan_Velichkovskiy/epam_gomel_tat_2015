package com.epam.gomel.tat.l9.service;

import com.epam.gomel.tat.l9.bo.file.Attach;
import com.epam.gomel.tat.l9.config.GlobalConfig;
import com.epam.gomel.tat.l9.exception.FileComparisonException;
import com.epam.gomel.tat.l9.pages.PassportPage;
import com.epam.gomel.tat.l9.pages.TrashPage;
import com.epam.gomel.tat.l9.pages.YandexDiskPage;
import com.epam.gomel.tat.l9.reporting.Logger;
import com.epam.gomel.tat.l9.utils.FileUtil;

import java.io.IOException;

import static com.epam.gomel.tat.l9.reporting.Constants.DiskOperationMessagePatterns.*;

/**
 * Created by Bahdan_Velichkouski on 4/3/2015.
 */
public class YaDiskGuiService {
    public void uploadFile(Attach attach) {
        PassportPage passportPage = new PassportPage();
        YandexDiskPage diskPage = passportPage.open().openYaDisk();
        diskPage.closePopup();
        diskPage.uploadFile(attach.getPath());
        diskPage.downloadFile(attach.getName());
        compareFiles(attach.getPath(), GlobalConfig.getInstance().getDownloadDirPath() + attach.getName());
    }

    public void moveFileToTrash(Attach attach) {
        YandexDiskPage diskPage = new YandexDiskPage();
        diskPage.moveFileToTrash(attach.getName());
    }

    public void restoreFileFromTrash(Attach attach) {
        YandexDiskPage diskPage = new YandexDiskPage();
        TrashPage trashPage = diskPage.openTrashPage();
        trashPage.restoreFileFromTrash(attach.getName());
        diskPage.open();
    }

    private void compareFiles(String initialFilePath, String downloadedFilePath) {
        try {
            if (!FileUtil.compareFiles(initialFilePath, downloadedFilePath)) {
                throw new FileComparisonException(String.format(ERROR_FILE_CONTENTS_NOT_IDENTICAL, initialFilePath, downloadedFilePath));
            }
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        } catch (FileComparisonException e) {
            Logger.error(e.getMessage(), e);
        }
    }
}
