package com.epam.gomel.tat.l9.bo.common;

/**
 * Created by user on 20.03.15.
 */
public class AccountBuilder {

    public static Account getDefaultAccount() {
        return DefaultAccounts.DEFAULT_MAIL.getAccount();
    }

    public static Account getDefaultAccountWithWrongPassword() {
        return DefaultAccounts.DEFAULT_MAIL_WRONG_PASSWORD.getAccount();
    }
}
