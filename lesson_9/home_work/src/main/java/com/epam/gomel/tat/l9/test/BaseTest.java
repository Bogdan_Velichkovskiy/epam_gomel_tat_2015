package com.epam.gomel.tat.l9.test;

import com.epam.gomel.tat.l9.bo.common.Account;
import com.epam.gomel.tat.l9.bo.common.AccountBuilder;
import com.epam.gomel.tat.l9.service.LoginGuiService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Created by vebogdan on 31.03.15.
 */
public class BaseTest {
    protected LoginGuiService loginGuiService = new LoginGuiService();
    protected Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account")
    public void loginToAccount() {
        loginGuiService.loginToAccount(defaultAccount);
    }

}
