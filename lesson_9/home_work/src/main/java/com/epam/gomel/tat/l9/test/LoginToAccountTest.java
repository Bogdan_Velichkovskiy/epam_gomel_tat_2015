package com.epam.gomel.tat.l9.test;

import com.epam.gomel.tat.l9.bo.common.Account;
import com.epam.gomel.tat.l9.bo.common.AccountBuilder;
import com.epam.gomel.tat.l9.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by user on 20.03.15.
 */
public class LoginToAccountTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @Test(description = "Login to account")
    public void loginToAccount() {
        loginGuiService.loginToAccount(defaultAccount);
    }
}
