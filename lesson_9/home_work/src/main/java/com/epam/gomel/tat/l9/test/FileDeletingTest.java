package com.epam.gomel.tat.l9.test;

import com.epam.gomel.tat.l9.bo.file.Attach;
import com.epam.gomel.tat.l9.config.GlobalConfig;
import com.epam.gomel.tat.l9.service.YaDiskGuiService;
import com.epam.gomel.tat.l9.utils.FileUtil;
import org.testng.annotations.Test;

/**
 * Created by Bahdan_Velichkouski on 4/7/2015.
 */
public class FileDeletingTest extends BaseTest {
    private YaDiskGuiService diskGuiService = new YaDiskGuiService();
    private Attach attach = null;

    @Test(description = "Upload file to disk")
    public void uploadFile() {
        attach = new Attach(FileUtil.generateFileInDir(GlobalConfig.getInstance().getUploadDirPath()));
        diskGuiService.uploadFile(attach);
    }

    @Test(description = "Move file to trash", dependsOnMethods = "uploadFile")
    public void moveFileToTrash() {
        diskGuiService.moveFileToTrash(attach);
    }
}
