package com.epam.gomel.tat.l9.reporting.Constants;

/**
 * Created by user on 24.03.15.
 */
public class CommonOperationsMessagePatterns {
    public static final String ADDING_LISTENER_TO_TESTS = "Adding listener to tests.";
    public static final String PARSING_ARGUMENTS = "Parsing arguments.";
    public static final String ADDING_SUITES_TO_TESTS = "Adding suites to tests.";
    public static final String RUNNING_TESTS = "Running tests.";
    public static final String CONFIG_START = "CONFIG START : %s. %s";
    public static final String CONFIG_SUCCESS = "CONFIG SUCCESS : %s. %s";
    public static final String CONFIG_FAILED = "CONFIG FAILED : %s. %s";
    public static final String CONFIG_SKIPPED = "CONFIG SKIPPED : %s. %s";
    public static final String TEST_METHOD_START = "TEST METHOD START : %s. %s";
    public static final String TEST_METHOD_SUCCESS = "TEST METHOD SUCCESS : %s. %s";
    public static final String TEST_METHOD_FAILED = "TEST METHOD FAILED : %s. %s";
    public static final String TEST_METHOD_SKIPPED = "TEST METHOD SKIPPED : %s. %s";
    public static final String START = "START : %s";
    public static final String FINISH = "FINISH : %s";
}
