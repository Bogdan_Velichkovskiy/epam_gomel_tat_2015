package com.epam.gomel.tat.l9.bo.file;

/**
 * Created by Bahdan_Velichkouski on 4/7/2015.
 */
public class Attach {
    private String path;
    private String name;

    public Attach(String path) {
        this.path = path;
        name = path.substring(path.lastIndexOf("\\") + 1);
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }
}
