package com.epam.gomel.tat.l9.utils;

import com.epam.gomel.tat.l9.reporting.Logger;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by user on 18.03.15.
 */
public class FileUtil {
    public static boolean compareFiles(String filePath1, String filePath2) throws IOException {
        return FileUtils.contentEquals(new File(filePath1), new File(filePath2));
    }

    public static String generateFileInDir(String dir)  {
        final String fileExt = ".txt";
        String filePath = dir + RandomUtils.getRandomString(10) + fileExt;
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(filePath);
            fillFileRandomContent(pw);
        } catch (FileNotFoundException e) {
            Logger.error(e.getMessage());
        } finally {
            if (pw !=null) {
                pw.close();
            }
        }

        return filePath;
    }

    private static void fillFileRandomContent(PrintWriter pw) {
        for (int i = 1; i <= 10; i++) {
            pw.write(RandomUtils.getRandomString(i)+"\n");
        }
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        final File jarFile = new File(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String path = null;
        try {
            if(jarFile.isFile()) {  // Run with JAR file
                List<String> listFiles = decompressResourceFromJAR(resourceFileLocalPath);
                path = listFiles.get(0);
            } else { // Run with IDE
                URL url = FileUtil.class.getClassLoader().getResource(resourceFileLocalPath);
                File file = new File(url.getPath());
                path = file.getCanonicalPath();
            }
        } catch (IOException e) {
            Logger.error(e.getMessage());
        } finally {
            return path;
        }
    }

    public static List<String> decompressResourceFromJAR(String folder) throws IOException {
        List<String> listFiles = new ArrayList<String>();
        String destPath = FileUtils.getTempDirectoryPath() + File.separator;
        JarFile jarFile = new JarFile(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        Enumeration<JarEntry> enums = jarFile.entries();
        while (enums.hasMoreElements()) {
            JarEntry entry = enums.nextElement();
            if (entry.getName().startsWith(folder)) {
                String filePath = destPath + entry.getName();
                File toWrite = new File(filePath);
                if (entry.isDirectory()) {
                    toWrite.mkdirs();
                    continue;
                }
                listFiles.add(filePath);
                if (toWrite.exists()) {
                    continue;
                }
                InputStream in = new BufferedInputStream(jarFile.getInputStream(entry));
                OutputStream out = new BufferedOutputStream(new FileOutputStream(toWrite));
                byte[] buffer = new byte[2048];
                for (;;) {
                    int nBytes = in.read(buffer);
                    if (nBytes <= 0) {
                        break;
                    }
                    out.write(buffer, 0, nBytes);
                }
                out.flush();
                out.close();
                in.close();
            }
        }
        return listFiles;
    }

    public static List<String> getListFilesInFolder(String folder) throws IOException {
        List<String> listFiles = new ArrayList<String>();
        final File jarFile = new File(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        if(jarFile.isFile()) {  // Run with JAR file
            return decompressResourceFromJAR(folder);
        } else { // Run with IDE
            String path = getCanonicalPathToResourceFile(folder);
            File dir = new File(path);
            for (File nextFile : dir.listFiles()) {
                listFiles.add(nextFile.getCanonicalPath());
            }
        }
        return listFiles;
    }

    public static String createDirInTempAndGetPath(String dirName) {
        String dirPath = FileUtils.getTempDirectoryPath() + dirName;
        try {
            FileUtils.forceMkdir(new File(dirPath));
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }
        return dirPath;
    }
}
