package com.epam.gomel.tat.l9.exception;

/**
 * Created by Bahdan_Velichkouski on 4/7/2015.
 */
public class FileComparisonException extends Exception {
    public FileComparisonException() {
        super();
    }

    public FileComparisonException(String message) {
        super(message);
    }

    public FileComparisonException(Throwable cause) {
        super(cause);
    }

    public FileComparisonException(String message, Throwable cause) {
        super(message, cause);
    }
}
