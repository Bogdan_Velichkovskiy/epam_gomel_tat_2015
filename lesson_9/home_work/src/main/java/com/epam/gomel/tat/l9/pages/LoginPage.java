package com.epam.gomel.tat.l9.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class LoginPage extends AbstractPage {

    public static final String BASE_URL = "http://passport.yandex.ru";

    private static final By LOGIN_INPUT_LOCATOR = By.id("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.id("passwd");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//button[@type='submit']");

    public LoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public PassportPage login(String login, String password) {
        browser.clearElementText(LOGIN_INPUT_LOCATOR);
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.takeScreenshot();
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.takeScreenshot();
        browser.click(LOGIN_BUTTON_LOCATOR);
        browser.takeScreenshot();
        return new PassportPage();
    }
}
