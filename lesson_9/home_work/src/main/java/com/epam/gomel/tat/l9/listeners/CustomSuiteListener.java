package com.epam.gomel.tat.l9.listeners;

import com.epam.gomel.tat.l9.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by Bahdan_Velichkouski on 4/7/2015.
 */
public class CustomSuiteListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode().getAlias());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
