package com.epam.gomel.tat.l9.runner;

import com.epam.gomel.tat.l9.config.GlobalConfig;
import com.epam.gomel.tat.l9.listeners.CustomSuiteListener;
import com.epam.gomel.tat.l9.reporting.CustomTestNgListener;
import com.epam.gomel.tat.l9.reporting.Logger;
import com.epam.gomel.tat.l9.utils.FileUtil;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

import static com.epam.gomel.tat.l9.reporting.Constants.CommonOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class Runner {
    private static GlobalConfig config = null;

    public static void main(String[] args) {
        config = GlobalConfig.getInstance();
        new Runner().run(args);
    }

    private void run(String[] args) {

        CmdLineParser parser = new CmdLineParser(config);
        Logger.info(ADDING_LISTENER_TO_TESTS);
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());
        testNG.addListener(new CustomSuiteListener());

        try {
            Logger.info(PARSING_ARGUMENTS);
            parser.parseArgument(args);

            Logger.info(ADDING_SUITES_TO_TESTS);
            XmlSuite suite = new XmlSuite();
            suite.setName("MySuite");
            List<String> files = new ArrayList<>();
            if (config.getSuites().size() != 0) {
                files.addAll(config.getSuites());
            } else {
                List<String> suites = FileUtil.getListFilesInFolder("suites");
                files.addAll(suites);
            }
            suite.setSuiteFiles(files);
            List<XmlSuite> suites = new ArrayList<XmlSuite>();
            suites.add(suite);
            testNG.setXmlSuites(suites);
            Logger.info(RUNNING_TESTS);
            testNG.run();
        } catch (CmdLineException e) {
            Logger.error(e.getMessage(),e);
            parser.printUsage(System.err);
        } catch (Exception e) {
            Logger.error(e.getMessage(),e);
        }
    }

}
