package com.epam.gomel.tat.l9.pages;

import com.epam.gomel.tat.l9.ui.Browser;

/**
 * Created by user on 20.03.15.
 */
public abstract class AbstractPage {
    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }
}
