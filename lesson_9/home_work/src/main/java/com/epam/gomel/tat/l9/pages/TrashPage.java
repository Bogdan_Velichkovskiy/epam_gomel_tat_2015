package com.epam.gomel.tat.l9.pages;

import org.openqa.selenium.By;

/**
 * Created by Bahdan_Velichkouski on 4/7/2015.
 */
public class TrashPage extends AbstractPage {
    private static final By FILE_RESTORE_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");
    private static final By NOTIFY_LOCATOR = By.xpath("//div[@class='ns-view-notifications notifications ns-view-visible']");
    private static final String FILE_ICON_LOCATOR_PATTERN = "//div[@title='%s']//div[@class='nb-icon-resource__image nb-icon-resource__image_icon nb-icon-resource__image_icon-txt nb-icon-resource__image_file']";

    public TrashPage open() {
        browser.open("https://disk.yandex.ru/client/trash");
        return this;
    }

    public void restoreFileFromTrash(String fileName) {
        browser.click(By.xpath(String.format(FILE_ICON_LOCATOR_PATTERN, fileName)));
        browser.click(FILE_RESTORE_BUTTON_LOCATOR);
        browser.waitForVisible(NOTIFY_LOCATOR);
    }
}
