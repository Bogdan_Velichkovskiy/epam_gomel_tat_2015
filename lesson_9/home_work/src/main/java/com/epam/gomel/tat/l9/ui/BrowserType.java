package com.epam.gomel.tat.l9.ui;

/**
 * Created by user on 28.03.15.
 */
public enum BrowserType {
    REMOTE("firefox*"),
    FIREFOX("firefox"),
    HTMLUNIT("htmlunit"),
    CHROME("chrome");

    private String alias;

    BrowserType(String alias) {
        this.alias = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for(BrowserType type: BrowserType.values()){
            if(type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }

}
