package com.epam.gomel.tat.l9.utils;

import static org.apache.commons.lang3.RandomStringUtils.*;

/**
 * Created by user on 22.03.15.
 */
public class RandomUtils {
    public static String getRandomString(int len) {
        return randomAlphabetic(len);
    }
}
