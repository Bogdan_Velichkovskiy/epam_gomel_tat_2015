package com.epam.gomel.tat.l9.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class PassportPage extends AbstractPage {

    private static final By DROPDOWN_LINKS_LOCATOR = By.xpath("//span[@class='b-dropdowna__switcher']//span[@class='b-link__inner']");
    private static final By USER_LOGIN_LOCATOR = By.xpath("//b[@class='b-user']");
    private static final By DISK_LINK_LOCATOR = By.xpath("//a[@href='http://disk.yandex.ru']");

    public PassportPage open() {
        browser.open(LoginPage.BASE_URL);
        return this;
    }

    public String getUserLogin() {
        return browser.getElementText(USER_LOGIN_LOCATOR);
    }

    public YandexDiskPage openYaDisk() {
        browser.click(DROPDOWN_LINKS_LOCATOR);
        browser.takeScreenshot();
        browser.click(DISK_LINK_LOCATOR);
        browser.takeScreenshot();
        return new YandexDiskPage();
    }
}
