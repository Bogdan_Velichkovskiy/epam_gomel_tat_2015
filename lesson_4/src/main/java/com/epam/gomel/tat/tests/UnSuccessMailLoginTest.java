package com.epam.gomel.tat.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by user on 19.03.15.
 */
public class UnSuccessMailLoginTest {

    public static final String BASE_URL = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//*[@class='js-messages']//.[@class='error-msg']");

    public static final String UNSUCCESS_LOGIN_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";
    public static final String ERROR_UNSUCCESS_LOGIN_TEXT_NOT_PRESENT = "The text 'Неправильная пара логин-пароль!' not present";

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;

    private WebDriver driver;
    private String userLogin = "testlogintat2015";
    private String incorrectUserPassword = "_test12345";

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "D:\\downloads\\");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml,text/plain,text/xml,image/jpeg");

        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(description = "Unsuccess login to yandex mail")
    public void unsuccessLogin() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(incorrectUserPassword);
        passInput.submit();

        WebElement errorMessage = driver.findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertEquals(errorMessage.getText(), UNSUCCESS_LOGIN_TEXT, ERROR_UNSUCCESS_LOGIN_TEXT_NOT_PRESENT);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.close();
    }
}
