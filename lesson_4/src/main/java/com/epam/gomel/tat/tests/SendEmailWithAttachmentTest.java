package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.utils.FileUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 19.03.15.
 */
public class SendEmailWithAttachmentTest {

    public static final String BASE_URL = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath(".//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By EMAIL_SUCCESS_SENDED__LOCATOR = By.xpath(".//div [@class='b-done-title']");
    public static final By INBOX_LINK_LOCATOR = By.xpath(".//a[@href='#inbox']");
    public static final By OUTBOX_LINK_LOCATOR = By.xpath(".//a[@href='#sent']");
    public static final By PROFILE_LINK_LOCATOR = By.xpath("//*[@class='header-user-name js-header-user-name']");
    public static final String MAIL_LINK_LOCATOR_PATTERN =
            "//div[@class='block-messages' and (@style='' or not(@style))]//a[.//span[@title='%s']]";
    public static final String DOWNLOAD_FILE_LINK_LOCATOR_PATTERN =
            "//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']";
    public static final String FILE_NAME_LOCATOR_PATTERN ="//div[@class='b-message-attachments_head']//span[@class='b-file__text']";

    public static final String ERROR_EMAIL_NOT_EQUALS = "Email not equals";

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    public static final int TIME_OUT_TEXT_PRESENT_SECONDS = 5;
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 30;

    public static final String HREF = "href";
    public static final String DOWNLOADS_DIR_PATH = "D:\\downloads\\";
    public static final String TEST_FILE_PATH = "D:\\TestFile.txt";
    public static final By ATTACH_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final String FILES_ARE_DIFFERENTS = "Contents of initial file differs from contents of downloaded file.";

    private WebDriver driver;
    private String userLogin = "testlogintat2015";
    private String userPassword = "test12345";
    private String userEmail = "testlogintat2015@yandex.ru";
    private String mailTo = "testlogintat2015@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOADS_DIR_PATH);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml,text/plain,text/xml,image/jpeg");

        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(description = "Success login to yandex mail")
    public void successLogin() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement profileLink = driver.findElement(PROFILE_LINK_LOCATOR);
        Assert.assertEquals(profileLink.getText().toLowerCase(), userEmail, ERROR_EMAIL_NOT_EQUALS);
    }

    @Test(description = "Success send email with attachment", dependsOnMethods = "successLogin")
    public void successSendEmailWithAttachment() throws InterruptedException, IOException {
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement attachFileButton = waitForElement(ATTACH_BUTTON_LOCATOR);
        attachFileButton.sendKeys(TEST_FILE_PATH);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        new WebDriverWait(driver, TIME_OUT_TEXT_PRESENT_SECONDS).until(
                ExpectedConditions.visibilityOfElementLocated(EMAIL_SUCCESS_SENDED__LOCATOR));

        WebElement sentLink = driver.findElement(OUTBOX_LINK_LOCATOR);
        sentLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));
        WebElement letterLink = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        driver.navigate().to(letterLink.getAttribute(HREF));

        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(DOWNLOAD_FILE_LINK_LOCATOR_PATTERN)));
        WebElement downloadLink = driver.findElement(By.xpath(DOWNLOAD_FILE_LINK_LOCATOR_PATTERN));
        driver.navigate().to(downloadLink.getAttribute(HREF));

        WebElement fileNameText = driver.findElement(By.xpath(FILE_NAME_LOCATOR_PATTERN));
        String downloadedFilePath = DOWNLOADS_DIR_PATH + fileNameText.getText();
        String initialFilePath = TEST_FILE_PATH;
        Assert.assertTrue(FileUtil.compareFiles(initialFilePath, downloadedFilePath), FILES_ARE_DIFFERENTS);

    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.close();
    }
}
