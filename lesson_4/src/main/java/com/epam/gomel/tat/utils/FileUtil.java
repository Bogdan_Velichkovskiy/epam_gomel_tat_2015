package com.epam.gomel.tat.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 18.03.15.
 */
public class FileUtil {
    public static boolean compareFiles(String filePath1, String filePath2) throws IOException {
        File file1 = new File(filePath1);
        File file2 = new File(filePath2);

        if (file1.length() != file2.length()) {
            return false;
        }

        List<String> firstList = null;
        List<String> secondList = null;
        firstList = getFileContent(filePath1);
        secondList = getFileContent(filePath2);

        for(String line : firstList) {
            for(String line2 : secondList) {
                if(!line.equals(line2)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static List<String> getFileContent(String filePath) throws IOException {
        BufferedReader br = null;
        List<String> result = new ArrayList<String>();
        String line;

        br = new BufferedReader(new FileReader(filePath));
        while ((line = br.readLine()) != null) {
            result.add(line);
        }
        br.close();
        return result;
    }
}
