package com.epam.gomel.tat.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by user on 19.03.15.
 */
public class SuccessMailLoginTest {

    public static final String BASE_URL = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By PROFILE_LINK_LOCATOR = By.xpath("//*[@class='header-user-name js-header-user-name']");

    public static final String ERROR_EMAIL_NOT_EQUALS = "Email not equals";

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;

    private WebDriver driver;
    private String userLogin = "testlogintat2015";
    private String userPassword = "test12345";
    private String userEmail = "testlogintat2015@yandex.ru";

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "D:\\downloads\\");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml,text/plain,text/xml,image/jpeg");

        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(description = "Success login to yandex mail")
    public void successLogin() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement profileLink = driver.findElement(PROFILE_LINK_LOCATOR);
        Assert.assertEquals(profileLink.getText().toLowerCase(), userEmail, ERROR_EMAIL_NOT_EQUALS);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.close();
    }
}
