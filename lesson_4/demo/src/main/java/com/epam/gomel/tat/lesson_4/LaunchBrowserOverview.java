package com.epam.gomel.tat.lesson_4;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class LaunchBrowserOverview {

    public static final String YANDEX_START_PAGE = "http://ya.ru";
    private WebDriver driver;

    @Test(description = "Firefox launch test")
    public void firefoxLocalLaunch() {
        driver = new FirefoxDriver();
        driver.get(YANDEX_START_PAGE);
    }

    @Test(description = "Chrome launch test")
    public void chromeLocalLaunch() {
        System.setProperty("webdriver.chrome.driver", "target/classes/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(YANDEX_START_PAGE);
    }

    @Test(description = "Firefox remote launch")
    public void firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 5544
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:5544/wd/hub"), DesiredCapabilities.firefox());
        driver.get(YANDEX_START_PAGE);
    }

    @Test(description = "Chrome remote launch")
    public void chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 5544 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:5544/wd/hub"), DesiredCapabilities.chrome());
        driver.get(YANDEX_START_PAGE);
    }

}
