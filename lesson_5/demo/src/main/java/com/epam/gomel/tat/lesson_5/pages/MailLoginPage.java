package com.epam.gomel.tat.lesson_5.pages;

import com.epam.gomel.tat.lesson_5.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailLoginPage extends AbstractBasePage {
    public static final String BASE_URL = "http://mail.yandex.ru";

    private static final By LOGIN_INPUT_LOCATOR = By.id("b-mail-domik-username11");
    private static final By PASSWORD_INPUT_LOCATOR = By.id("b-mail-domik-password11");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//input[@type='submit']");

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new MailboxBasePage();
    }

}
