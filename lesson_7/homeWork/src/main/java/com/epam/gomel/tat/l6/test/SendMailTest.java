package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.service.MailGuiService;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.epam.gomel.tat.l6.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by user on 20.03.15.
 */
public class SendMailTest extends BaseTest {
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = null;

    @Test(description = "Send mail")
    public void sendMail() throws FileNotFoundException {
        letter = generateAndGetLetter(defaultAccount.getEmail());
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in sent list", dependsOnMethods = "sendMail")
    public void checkMailInSentList() {
        mailGuiService.checkMailInSentList(letter);
    }
}
