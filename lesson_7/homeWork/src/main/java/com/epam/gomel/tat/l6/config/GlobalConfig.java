package com.epam.gomel.tat.l6.config;

import com.epam.gomel.tat.l6.cli.BrowserTypeEnumOptionHandler;
import com.epam.gomel.tat.l6.ui.BrowserType;
import com.epam.gomel.tat.l6.utils.FileUtil;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28.03.15.
 */
public class GlobalConfig {

    private static GlobalConfig instance;

    @Option(name = "-bt", handler = BrowserTypeEnumOptionHandler.class, usage = "browser type", required = true)
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-tc", usage = "thread count")
    private int threadCount = 1;

    @Option(name = "-mode", usage = "parallel mode: false, methods, classes, tests, suites")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-suites", handler = StringArrayOptionHandler.class, usage = "paths to suites")
    private List<String> suites;

    @Option(name = "-host", usage = "<IP | hostname>")
    private String host = "localhost";

    @Option(name = "-p", depends={"-host"}, usage = "port, default is 4444")
    private int port = 4444;

    private String uploadDirPath = null;
    private String downloadDirPath = null;

    private GlobalConfig() {
        suites = new ArrayList<String>();
        uploadDirPath = FileUtil.createDirInTempAndGetPath("Uploads") + File.separator;
        downloadDirPath = FileUtil.createDirInTempAndGetPath("Downloads") + File.separator;
    }

    public static synchronized GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public String getUploadDirPath() {
        return uploadDirPath;
    }

    public void setUploadDirPath(String uploadDirPath) {
        this.uploadDirPath = uploadDirPath;
    }

    public String getDownloadDirPath() {
        return downloadDirPath;
    }

    public void setDownloadDirPath(String downloadDirPath) {
        this.downloadDirPath = downloadDirPath;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "GlobalConfig{" +
                "browserType=" + browserType +
                ", threadCount=" + threadCount +
                ", parallelMode=" + parallelMode +
                ", suites=" + suites +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", uploadDirPath='" + uploadDirPath + '\'' +
                ", downloadDirPath='" + downloadDirPath + '\'' +
                '}';
    }
}
