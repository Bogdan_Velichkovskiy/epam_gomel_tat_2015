package com.epam.gomel.tat.l6.pages;

import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class MailboxBasePage extends AbstractPage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    private static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final By PROFILE_LINK_LOCATOR = By.xpath("//*[@class='header-user-name js-header-user-name']");
    private static final By LOGOUT_LINK_LOCATOR = By.xpath("//a[contains(@href,'retpath=http%3A%2F%2Fwww.yandex.by')]");
    private static final By PROFILE_LINK_LOCATOR1 = By.xpath("//a[@id='nb-1']");

    private static final String INBOX_TITLE = "Входящие";

    public MailboxBasePage open() {
        browser.open(MailLoginPage.BASE_URL);
        return new MailboxBasePage();
    }

    public MailInboxListPage openInboxPage() {
        if (!browser.getTitle().contains(INBOX_TITLE)) {
            browser.click(INBOX_LINK_LOCATOR);
            browser.waitForAjaxProcessed();
        }
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        browser.click(TRASH_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public String getUserEmail() {
        return browser.getElementText(PROFILE_LINK_LOCATOR);
    }

    public void logout() {
        browser.click(PROFILE_LINK_LOCATOR1);
        browser.click(LOGOUT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
    }
}
