package com.epam.gomel.tat.l6.runner;

import com.epam.gomel.tat.l6.config.GlobalConfig;
import com.epam.gomel.tat.l6.reporting.CustomTestNgListener;
import com.epam.gomel.tat.l6.reporting.Logger;
import com.epam.gomel.tat.l6.utils.FileUtil;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

import static com.epam.gomel.tat.l6.reporting.Constants.CommonOperationsMessagePatterns.*;

/**
 * Created by user on 20.03.15.
 */
public class Runner {
    private static GlobalConfig config = null;

    public static void main(String[] args) {
        config = GlobalConfig.getInstance();
        new Runner().run(args);
    }

    private void run(String[] args) {

        CmdLineParser parser = new CmdLineParser(config);
        Logger.info(ADDING_LISTENER_TO_TESTS);
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());

        try {
            Logger.info(PARSING_ARGUMENTS);
            parser.parseArgument(args);
            Logger.info(ADDING_SUITES_TO_TESTS);
            if (config.getSuites().size() != 0) {
                testNG.setXmlSuites(configureSuites(config.getSuites()));
            } else {
                List<String> suites = FileUtil.getListFilesInFolder("suites");
                testNG.setXmlSuites(configureSuites(suites));
            }
            Logger.info(RUNNING_TESTS);
            testNG.run();
        } catch (CmdLineException e) {
            Logger.error(e.getMessage(),e);
            parser.printUsage(System.err);
        } catch (Exception e) {
            Logger.error(e.getMessage(),e);
        }
    }

    private static List<XmlSuite> configureSuites(List<String> suiteFiles) {
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        try {
            for (String suiteFile : suiteFiles) {
                List<XmlSuite> suitesToConfigure = (List<XmlSuite>) (new Parser(suiteFile).parse());
                for (XmlSuite suiteToConfigure : suitesToConfigure) {
                    suiteToConfigure.setParallel(config.getParallelMode().getAlias());
                    suiteToConfigure.setThreadCount(config.getThreadCount());
                    suites.add(suiteToConfigure);
                }
            }
        }catch (Exception e) {
            Logger.error(e.getMessage(),e);
        }

        return suites;
    }
}
