package com.epam.gomel.tat.l6.cli;

import com.epam.gomel.tat.l6.ui.BrowserType;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.Messages;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

/**
 * Created by user on 28.03.15.
 */
public class BrowserTypeEnumOptionHandler extends OptionHandler<BrowserType> {

    public BrowserTypeEnumOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super BrowserType> setter) {
        super(parser, option, setter);
    }

    @Override
    public int parseArguments(Parameters parameters) throws CmdLineException {
        String valueStr = parameters.getParameter(0).toLowerCase();
        if (valueStr == null || valueStr.isEmpty()) {
            throw new CmdLineException(this.owner, Messages.ILLEGAL_CHAR, new String[]{valueStr});
        } else {
            this.setter.addValue(BrowserType.getTypeByAlias(valueStr));
            return 1;
        }
    }

    @Override
    public String getDefaultMetaVariable() {
        return null;
    }
}
