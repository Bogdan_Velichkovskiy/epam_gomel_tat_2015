package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Created by vebogdan on 31.03.15.
 */
public class BaseTest {
    protected LoginGuiService loginGuiService = new LoginGuiService();
    protected Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @AfterClass(description = "Logout...")
    public void logout() {
        loginGuiService.logout(defaultAccount);
    }
}
