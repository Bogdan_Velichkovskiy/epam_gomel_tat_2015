package com.epam.gomel.tat.l6.pages;

import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.config.GlobalConfig;
import org.openqa.selenium.By;

/**
 * Created by user on 20.03.15.
 */
public class LetterContentPage extends AbstractPage {
    private static final By TO_LOCATOR =
            By.xpath("//div[@class='b-message-head__field__content']/span[@class='b-message-head__contact']/span[@class='b-message-head__name']");
    private static final By SUBJECT_LOCATOR = By.xpath("//span[@class='js-message-subject js-invalid-drag-target']");
    private static final By MAIL_TEXT_LOCATOR =
            By.xpath("//div[@class='block-message-body-box']//div[@class='b-message-body__content']/p");
    private static final By ATTACH_FILE_LOCATOR = By.xpath("//span[@class='b-file__text']");
    private static final By TOGGLE_RECIEVERS_BUTTON_LOCATOR = By.xpath("//div[@class='b-message-head__toggler__show']");
    private static final By DOWNLOAD_FILE_BUTTON_LOCATOR =
            By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");

    public MailLetter getLetter() {
        if (browser.isElementVisible(TOGGLE_RECIEVERS_BUTTON_LOCATOR)) {
            browser.click(TOGGLE_RECIEVERS_BUTTON_LOCATOR);
        }
        String downloadedFilePath = GlobalConfig.getInstance().getDownloadDirPath() + browser.getElementText(ATTACH_FILE_LOCATOR);
        browser.goTo(browser.getElementAttribute(DOWNLOAD_FILE_BUTTON_LOCATOR, HREF));
        browser.waitForFileDownload(downloadedFilePath);

        return new MailLetter(browser.getElementText(TO_LOCATOR), browser.getElementText(SUBJECT_LOCATOR),
                browser.getElementText(MAIL_TEXT_LOCATOR), downloadedFilePath);
    }
}
