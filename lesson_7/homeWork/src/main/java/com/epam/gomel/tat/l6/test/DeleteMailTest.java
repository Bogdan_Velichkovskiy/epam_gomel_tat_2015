package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.mail.MailLetter;
import com.epam.gomel.tat.l6.service.MailGuiService;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.epam.gomel.tat.l6.bo.mail.LetterBuilder.generateAndGetLetter;

/**
 * Created by user on 21.03.15.
 */
public class DeleteMailTest extends BaseTest {
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = null;

    @Test(description = "Send mail")
    public void sendMail() throws FileNotFoundException {
        letter = generateAndGetLetter(defaultAccount.getEmail());
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Delete mail", dependsOnMethods = "sendMail")
    public void deleteMail() {
        mailGuiService.deleteMailFromInbox(letter);
    }

    @Test(description = "Check mail in trash list", dependsOnMethods = "deleteMail")
    public void checkMailInTrashList() {
        mailGuiService.checkMailInTrashList(letter);
    }
}
