package com.epam.gomel.tat.l6.test;

import com.epam.gomel.tat.l6.bo.common.Account;
import com.epam.gomel.tat.l6.bo.common.AccountBuilder;
import com.epam.gomel.tat.l6.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by user on 20.03.15.
 */
public class UnsuccessLoginToMailboxTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccountWithWrongPassword();

    @Test(description = "Unsuccess login to account mailbox")
    public void loginToAccount() {
        loginGuiService.unsuccessLoginToAccountMailbox(defaultAccount);
    }
}
